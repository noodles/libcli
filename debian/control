Source: libcli
Priority: optional
Section: libs
Maintainer: Jonathan McDowell <noodles@earth.li>
Build-Depends: debhelper-compat (= 13),
	dpkg-dev (>= 1.22.5)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/dparrish/libcli
Vcs-Browser: https://salsa.debian.org/noodles/libcli
Vcs-Git: https://salsa.debian.org/noodles/libcli.git

Package: libcli-dev
Section: libdevel
Architecture: any
Depends: libcli1.10t64 (= ${binary:Version}), ${misc:Depends}
Multi-Arch: same
Description: emulates a cisco style telnet command-line interface (dev files)
 libcli provides a consistent Cisco style command-line environment for
 remote clients, with a few common features between every implemtation.
 .
 The library is not accessed by itself, rather the software which uses
 it listens on a defined port for a Telnet connection. This connection
 is handed off to libcli for processing.
 .
 libcli includes support for command history, command line editing and
 filtering of command output.
 .
 This package contains the files necessary for developing applications
 with libcli.

Package: libcli1.10t64
Breaks: libcli1.10 (<< ${source:Version})
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Conflicts: libcli1
Provides: libcli1, ${t64:Provides}
Replaces: libcli1.10, libcli1
Description: emulates a cisco style telnet command-line interface
 libcli provides a consistent Cisco style command-line environment for
 remote clients, with a few common features between every implemtation.
 .
 The library is not accessed by itself, rather the software which uses
 it listens on a defined port for a Telnet connection. This connection
 is handed off to libcli for processing.
 .
 libcli includes support for command history, command line editing and
 filtering of command output.
