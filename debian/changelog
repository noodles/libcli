libcli (1.10.7-2) unstable; urgency=medium

  [ Debian Janitor]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on dh-exec.

  [ Jonathan McDowell ]
  * Fix compilation with GCC 14. Closes: #1075158
  * Switch to debhelper-compat level 13
  * Drop dh-exec dependency
  * Set Rules-Requires-Root to no
  * Bump Standards-Version to 4.7.0

 -- Jonathan McDowell <noodles@earth.li>  Fri, 26 Jul 2024 19:50:26 +0100

libcli (1.10.7-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062252

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 10:28:25 +0000

libcli (1.10.7-1) unstable; urgency=low

  * New upstream release.
  * Fix Vcs-Git link to point to Salsa

 -- Jonathan McDowell <noodles@earth.li>  Sun, 26 Sep 2021 11:29:16 +0100

libcli (1.10.4-1) unstable; urgency=low

  * New upstream release.
  * Update watch file to cope with V/v in version tag.

 -- Jonathan McDowell <noodles@earth.li>  Mon, 18 May 2020 18:01:56 +0100

libcli (1.10.2-1) unstable; urgency=low

  * New upstream release.

 -- Jonathan McDowell <noodles@earth.li>  Sun, 01 Dec 2019 09:39:57 +0000

libcli (1.10.0-1) unstable; urgency=low

  * New upstream release.
    * Remove no longer necessary GCC 5 build patch.
  * Update watch file to use github directly instead of githubredir.
  * Bump debhelper to compat level 10.
  * Mark libcli-dev as Multi-Arch: same
  * Switch to DH.
  * Bump Standards-Version to 4.4.0 (no further changes from above)

 -- Jonathan McDowell <noodles@earth.li>  Sat, 27 Jul 2019 14:47:26 -0300

libcli (1.9.7-2) unstable; urgency=low

  * Fix compilation with GCC 5. Thanks to James Cowgill. (closes: #777941)

 -- Jonathan McDowell <noodles@earth.li>  Fri, 26 Jun 2015 12:41:28 +0100

libcli (1.9.7-1) unstable; urgency=low

  * New upstream release.
  * Fix spelling mistake in description.
  * Bump Standards-Version to 3.9.5.0
    * Add symbols file covering 1.9.1 + 1.9.6.

 -- Jonathan McDowell <noodles@earth.li>  Sun, 23 Feb 2014 15:14:36 +0000

libcli (1.9.6-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper to compat level 8.
  * Move to source format 3.0 (quilt).
  * Ship clitest.c as example with -dev package. (closes: #550169)
  * Enable hardening flags.
  * Enabled multiarch.
  * Bump Standards-Version to 3.9.3.0 (no changes).
  * Add Vcs-Git header to debian/control
  * Update watch file.

 -- Jonathan McDowell <noodles@earth.li>  Wed, 06 Jun 2012 22:05:07 -0700

libcli (1.9.1~20080302-1) unstable; urgency=low

  * New upstream release (1.9.0 + CVS fixes).
  * Alter watch file to use qa.debian.org redirector. (closes: #450183)
  * Lintian cleanups:
    * Don't ignore make clean errors.
    * Change from ${Source-Version} to ${binary:Version}
    * Fix copyright declaration.
    * Bump standards version (no other changes).

 -- Jonathan McDowell <noodles@earth.li>  Sun, 02 Mar 2008 10:11:29 +0000

libcli (1.8.8-2) unstable; urgency=low

  * Add Provides/Replaces/Conflicts for libcli1; we're providing the same
    ABI, just with a correctly named package.

 -- Jonathan McDowell <noodles@earth.li>  Tue, 29 May 2007 15:04:43 +0100

libcli (1.8.8-1) unstable; urgency=low

  * New upstream release.
  * Fix binary package name to match soname (libcli1.8).
  * Bump Standards-Version to 3.7.2.2 (no changes).

 -- Jonathan McDowell <noodles@earth.li>  Mon, 21 May 2007 17:37:31 +0100

libcli (1.8.6-1) unstable; urgency=low

  * New upstream release.

 -- Jonathan McDowell <noodles@earth.li>  Mon, 20 Mar 2006 16:47:49 +0000

libcli (1.8.5-1) unstable; urgency=low

  * New upstream release.

 -- Jonathan McDowell <noodles@earth.li>  Thu, 05 May 2005 12:34:24 +0100

libcli (1.8.4-1) unstable; urgency=low

  * New upstream release.
  * Fix up watch file.
  * Lower case first letter of Description to keep lintian happy.

 -- Jonathan McDowell <noodles@earth.li>  Thu, 06 Jan 2005 14:08:17 +0000

libcli (1.8.2-1) unstable; urgency=low

  * New upstream release.
  * Initial Debian upload (closes: #280611)

 -- Jonathan McDowell <noodles@earth.li>  Tue, 16 Nov 2004 12:25:56 +0000

libcli (1.8.1-1) unstable; urgency=low

  * New upstream release.
  * Add watch file.

 -- Jonathan McDowell <noodles@earth.li>  Wed,  1 Sep 2004 10:55:56 +0100

libcli (1.6.2-1) unstable; urgency=low

  * Initial packaging.

 -- Jonathan McDowell <noodles@earth.li>  Wed, 30 Jun 2004 12:13:31 +0100

